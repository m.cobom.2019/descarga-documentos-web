##Ejercicio 16.7
##Autor: Mario Cobo Martínez; con código reutilizado de la asignatura Servicios Telemáticos de Ingeniería Telemática de la URJC.
import urllib.request


class Robot:

    # Defino la url que es la que le vamos a pasar para leer y
    # un booleano recuperado para saber si se ha descargado ya la url o no.
    def __init__(self, url):
        self.url = url
        self.recuperado = False
        print(self.url)

    # Función en la que vemos primero si se ha descargado la url,
    # si no, la descarga, la lee y cambia el booleano recuperado a True,
    # ya que hemos leido la url.
    def retrieve(self):
        if (self.recuperado == False):
            print("Descargando url")
            f = urllib.request.urlopen(self.url)
            self.content = f.read().decode('utf-8')
            self.recuperado = True

    # Función para sacar el contenido de la url leida en la función
    # retrieve.
    def content(self):
        self.retrieve()
        return self.content

    # Funcion imprimir en pantalla lo leido anteriormente(la url)
    def show(self):
        print(self.content())


class Cache:

    # inilizamos la cache
    def __init__(self):
        self.cache = {}

    # si la url no ha sido leida(no está dentro de la cache
    # creamos un bot que la lea
    def retrieve(self, url):
        if url not in self.cache:
            bot = Robot(url=url)
            self.cache[url] = bot

    # sacamos el contenido leido en la función anterior
    def content(self, url):
        self.retrieve(url)
        return self.cache[url].content()

    # Función para imprimir la url leida
    def show(self, url):
        print(self.content(url))

    # Función para imprimir todas la url leidas que contiene la cache
    def show_all(self):
        for url in self.cache:
            print(url)


##Test proporcionado:
if __name__ == '__main__':
    print("Test Robot class")
    r = Robot('https://www.youtube.com')
    print(r.url)
    r.show()
    r.retrieve()
    r.retrieve()

    print("Test Cache class")
    c = Cache()
    c.retrieve('https://www.wikipedia.org')
    c.show('https://es.wikipedia.org/wiki/Universidad_Rey_Juan_Carlos')
    c.show_all()